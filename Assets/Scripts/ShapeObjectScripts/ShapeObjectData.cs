using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Data class for keeping the data together and comparisons
[System.Serializable]
public class ShapeObjectData
{
    public ShapeObjectData(Sprite _sprite)
    {
        sprite = _sprite;
    }

    // Currently only saves the sprite data.
    // Could also contain stuff like IDs, scriptableobjects, particlesystems etc.
    public Sprite sprite; // Visual representation
    
    public bool IsSameType(ShapeObjectData other)
    {
        // Naive but working comparison is to compare if the sprites are the same asset
        // Maybe a better alternative might be to save an ID in this data and compare those instead
        return other.sprite == sprite;
    }
}
