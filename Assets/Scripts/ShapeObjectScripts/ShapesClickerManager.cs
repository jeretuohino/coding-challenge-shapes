using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

// Struct for double checking overlapping spawns
struct testPos
{
    public Vector3 pos;
    public bool success;
    public testPos(Vector3 _pos, bool _success)
    {
        pos = _pos;
        success = _success;
    }
}

public class ShapesClickerManager : MonoBehaviour
{
    [Header("THIS NEEDS TO BE REFERENCED FOR BUILD SUPPORT")]
    public GameSettingsScriptableObject gameSettings;
    float gamestartedTime;
    private float lastSpawnTime;
    private float lastTypeRefresh;
    bool gameOngoing = false;

    [Header("Object Spawning")]
    public Vector2 spawnAreaRange = new Vector2(5,5);
    public float spawnCheckRadius = .5f;
    public Transform playArea;
    public GameObject shapeobjectGO;
    public int maxSpawnTestLoops = 10;
    List<testPos> testPosList = new List<testPos>();

    [Header("UI Variables")]
    // Buttons
    [SerializeField]Button startButton;
    [SerializeField]Button stopButton;
    // Info Texts
    [SerializeField]Text timeLeftText;

    public StatsUI statsUI;

    [Header("ShapeObject Lists")]
    // Create a list of available shapeObjectTypes
    // We have ShapeObject as its own class to allow more variables and common methods
    [SerializeField]List<ShapeObjectData> shapeObjectTypes;
    
    // For optimizing heap, implement Object Pooling for ShapeObjects
    
    // Correct Type Variables
    public List<Image> correctTypeImages = new List<Image>();
    List<ShapeObjectData> correctTypes = new List<ShapeObjectData>();

    // Draw tested spawn points when this gameobject is selected
    void OnDrawGizmosSelected() 
    {
        foreach(testPos testpos in testPosList)
        {
            Gizmos.color = testpos.success ? Color.green : Color.red;
            Gizmos.DrawSphere(testpos.pos, spawnCheckRadius);
        }

        // Draw the spawn area in Editor for easier SpawnArea adjustments

        Gizmos.color = Color.green;

        // Multiply the spawnRange by 2, since DrawWireCube() wants finalized size, not radius
        Vector3 wirecubeSize = new Vector3(spawnAreaRange.x*2,0,spawnAreaRange.y*2);
        if(playArea != null)Gizmos.DrawWireCube(playArea.position, new Vector3(spawnAreaRange.x*2,0,spawnAreaRange.y*2));
    }
    private void Update()
    {
        if(!gameOngoing) return;
        
        // Check if the interval between object spawning is long enough
        if(Time.time - lastSpawnTime > gameSettings.objectSpawnInterval)
        {
            StartCoroutine(SpawnObject());
            lastSpawnTime = Time.time;
        }
        // Check if the interval between updating correct types is long enough
        if(Time.time - lastTypeRefresh > gameSettings.refreshTypeTime)
        {
            RefreshCorrectTypes();
            lastTypeRefresh = Time.time;
        }

        // Update Timeleft
        if(Time.time - gamestartedTime > gameSettings.gameLength)
        {
            timeLeftText.text = "0.00s";
            stopButton.interactable = false;
            startButton.interactable = true;
            StopGame();
        }
        else
        {
            timeLeftText.text = "Time Left: " + (gameSettings.gameLength - (Time.time - gamestartedTime)).ToString("F2") + 's';
        }
    }

    // Start and Stop game functions
    public void StartGame()
    {
        // Initialize the timers
        lastSpawnTime = Time.time;
        gamestartedTime = Time.time;

        // Set the boolean for true so we can start the game
        gameOngoing = true;
        
        RefreshCorrectTypes();

        // Reset score values
        statsUI.ResetStats();
        statsUI.ShowStats(false);
    }
    public void StopGame()
    {
        // Set the boolean to stop updates
        gameOngoing = false;
        statsUI.ShowStats(true);

        // Clean up every ShapeObject
        ShapeObject[] shapeObjects = FindObjectsOfType<ShapeObject>();
        foreach(ShapeObject shape in shapeObjects)
        {
            Destroy(shape.gameObject);
        }
    }


    // Spawn Object
    public IEnumerator SpawnObject()
    {
        // Clear testposList to see per tested points per spawn
        testPosList.Clear();

        Vector3 spawnPoint = Vector3.zero;
        int loop = 0;

        // Loop this till we have either:
        //      Found a spawn point with enough room
        //      Hit the maxSpawnTestLoops, in which case we just ignore the overlaps
        while(spawnPoint == Vector3.zero || loop > maxSpawnTestLoops)
        {
            loop++;

            Vector2 randomPoint = GetRandomPoint(spawnAreaRange.x, spawnAreaRange.y);
            Vector3 proposedPoint = new Vector3(playArea.position.x + randomPoint.x, playArea.position.y, playArea.position.z + randomPoint.y);
            
            // Check for eligiility
            // Eligible: Within spawn area, doesn't collide with an already existing object 
            LayerMask layermask = LayerMask.GetMask("ShapeObject");

            bool eligible = loop <= maxSpawnTestLoops ?!Physics.CheckSphere(proposedPoint, spawnCheckRadius, layermask) : true;

            // Add tested point to a Debug List
            testPosList.Add(new testPos(proposedPoint, eligible));

            if(eligible) spawnPoint = proposedPoint; 
            
            yield return new WaitForEndOfFrame();
        } 

        // Spawn the object
        GameObject newShapeObject = Instantiate(shapeobjectGO, spawnPoint, Quaternion.identity, null);

        // Initialize the Data values
        ShapeObjectData data = shapeObjectTypes[Random.Range(0,shapeObjectTypes.Count)];
        newShapeObject.GetComponent<ShapeObject>().Init(data, this);
        yield return null;
    }

    Vector2 GetRandomPoint(float radiusX, float radiusY)
    {
        // Get a random X and Y position from the spawnareaRange
        float xpos = Random.Range(-spawnAreaRange.x, spawnAreaRange.x);
        float ypos = Random.Range(-spawnAreaRange.y, spawnAreaRange.y);
        Vector2 boxPoint = new Vector2(xpos,ypos);

        return boxPoint;
    }

    void RefreshCorrectTypes()
    {
        // We clear the previous correct lists
        correctTypes.Clear();

        // Error Check
        if(shapeObjectTypes.Count < 1) 
        {
            Debug.LogError("The available shapeObjectTypes list's length is 0");
            return;
        }
        else if(shapeObjectTypes.Count < correctTypeImages.Count)
        {
            Debug.LogError("There are more correct types than available types");
            return;
        }

        // Make temporary array of available types so we don't get duplicates
        List<ShapeObjectData> availableTypes = new List<ShapeObjectData>(shapeObjectTypes);

        // For loop to add as many correct types we want to
        for(int i = 0; i < correctTypeImages.Count; i++)
        {
            int typeID = Random.Range(0,availableTypes.Count);
            correctTypes.Add(availableTypes[typeID]);
            availableTypes.RemoveAt(typeID);
            
            correctTypeImages[i].sprite = correctTypes[i].sprite;
        }
    }

    public bool IsCorrect(ShapeObjectData data)
    {
        // Check if shape is correct
        bool isCorrect = false;
        for (int i = 0; i < correctTypes.Count; i++)
        {
            isCorrect = correctTypes[i].IsSameType(data);
            if(isCorrect) break;
        }

        return isCorrect;
    }
}
