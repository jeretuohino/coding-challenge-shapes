using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeObject : MonoBehaviour
{
    ShapesClickerManager manager;
    ShapeObjectData data;
    [SerializeField]SpriteRenderer spriteRenderer;
    [SerializeField]ParticleSystem pSystem;
    float lifetime = 5f;
    float spawnTime;
    bool dying = false;
    private void Update() 
    {
        if(Time.time - spawnTime > lifetime && !dying)
        {
            manager.statsUI.RegisterObjectDespawn(manager.IsCorrect(data), false);
            Destroy(gameObject);
        }
    }

    public void Init(ShapeObjectData _data, ShapesClickerManager _manager)
    {
        manager = _manager;
        data = _data;
        spriteRenderer.sprite = _data.sprite;
        lifetime = GameSettingsScriptableObject.Instance.objectLifetime;
        spawnTime = Time.time;
    }

    // Simple method to get mouseOver without raycasting
    private void OnMouseOver() 
    {
        if(Input.GetMouseButtonDown(0) && !dying)
        {
            // Mark this as dying so we don't destroy the object mid-particles
            dying = true;

            // Check if this is the correct type
            bool isCorrect = manager.IsCorrect(data);
            
            // Disable the spriterenderer
            spriteRenderer.enabled = false;

            // Play the particles and Audio if needed
            ParticleSystem.MainModule main = pSystem.main;
            main.startColor = isCorrect? Color.green : Color.red;
            pSystem.Play();

            // Register this Object despawning to the GameManager
            manager.statsUI.RegisterObjectDespawn(isCorrect, true);

            // Destroy this gameobject in 1 second
            Destroy(gameObject, 1);
        }
    }
}
