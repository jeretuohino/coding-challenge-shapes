using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Class for Decimal Formatting Dictionary (int == format)
class decimalFormat
{
    public decimalFormat(string _format, int _digits)
    {
        format = _format;
        digits = _digits;
    }

    public string format;
    public int digits;
}

public class VariableBar: MonoBehaviour
{
    // External References
    public Slider slider;
    public Text valueText; 

    // Decimal variables
    public enum DECIMALS {zero,one,two,three};
    public DECIMALS decimals = DECIMALS.two;
     
    // Dictionary to allow decimal formatting with both Round(digits) values and ToString("Fn") to force n digits
    Dictionary<DECIMALS, decimalFormat> decimalFormatList = new Dictionary<DECIMALS, decimalFormat>()
    {
        {DECIMALS.zero, new decimalFormat("",0)},
        {DECIMALS.one, new decimalFormat("F1",1)},
        {DECIMALS.two, new decimalFormat("F2",2)},
        {DECIMALS.three, new decimalFormat("F3",3)}
    };
    public void UpdateText()
    {
        // Update the text and limit the Slider value to n decimals based on decimals enum
        slider.value = Round(slider.value, decimalFormatList[decimals].digits);
        valueText.text = slider.value.ToString(decimalFormatList[decimals].format);
    }


    // Method to allow external sources to force the change of the value
    public void UpdateValue(float value)
    {
        slider.value = value;
        UpdateText();
    }

    // Calculate value to show n amount of decimals
    public float Round(float value, int digits)
    {
        float mult = Mathf.Pow(10.0f, (float)digits);
        return Mathf.Round(value * mult) / mult;
    }
}
