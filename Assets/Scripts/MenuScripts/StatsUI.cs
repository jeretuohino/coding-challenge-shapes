using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsUI : MonoBehaviour
{
    // Score Values
    int totalcorrectObjects = 0;
    int hitCorrectObjects = 0;
    int hitWrongObjects = 0;

    // Value Texts

    [SerializeField] Text accuracyText;
    [SerializeField] Text correctObjectsText;
    [SerializeField] Text hitCorrectObjectsText;
    [SerializeField] Text hitWrongObjectsText;
    [SerializeField] Text missedCorrectObjectsText;

    public void ShowStats(bool show)
    {
        UpdateStats();
        gameObject.SetActive(show);
    }

    void UpdateStats()
    {
        // Update the Stat Values to match
        correctObjectsText.text = totalcorrectObjects.ToString();
        hitCorrectObjectsText.text = hitCorrectObjects.ToString();
        hitWrongObjectsText.text = hitWrongObjects.ToString();
        missedCorrectObjectsText.text = (totalcorrectObjects-hitCorrectObjects).ToString();
        accuracyText.text = string.Format("{0}%",(((float)hitCorrectObjects/(Mathf.Max(1f,totalcorrectObjects + hitWrongObjects)))*100).ToString("F2"));
    }

    public void RegisterObjectDespawn(bool isCorrect, bool userClicked)
    {
        if(isCorrect) totalcorrectObjects++;                    // Add ALL CorrectObjects to count
        if(isCorrect && userClicked) hitCorrectObjects++;       // Add if hitCorrect
        if(!isCorrect && userClicked) hitWrongObjects++;        // Add if hitWrong
    }

    public void ResetStats()
    {
        totalcorrectObjects = 0;
        hitCorrectObjects = 0;
        hitWrongObjects = 0;
    }
}
