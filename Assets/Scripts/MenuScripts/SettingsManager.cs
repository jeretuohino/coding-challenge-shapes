using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsManager : MonoBehaviour
{
    GameSettingsScriptableObject gameSettings;

    // Variable Bars
    [SerializeField]VariableBar VBSpawnFrequency;
    [SerializeField]VariableBar VBObjectLifetime;
    [SerializeField]VariableBar VBGameLength;
    [SerializeField]VariableBar VBRefreshTypes;

    void Awake() 
    {
        // Get the game settings
        gameSettings = GameSettingsScriptableObject.Instance;

        // Initialize the VariableBars with gameSetting values
        VBSpawnFrequency.UpdateValue(gameSettings.objectSpawnInterval);
        VBObjectLifetime.UpdateValue(gameSettings.objectLifetime);
        VBGameLength.UpdateValue(gameSettings.gameLength);
        VBRefreshTypes.UpdateValue(gameSettings.refreshTypeTime);
    }


    // UI Variable Update Methods
    public void UpdateSpawnFreq()
    {
        gameSettings.objectSpawnInterval = VBSpawnFrequency.slider.value;
    }
    public void UpdateObjectLifetime()
    {
        gameSettings.objectLifetime = VBObjectLifetime.slider.value;
    }
    public void UpdateGameLength()
    {
        gameSettings.gameLength = VBGameLength.slider.value;
    }
    public void UpdateRefreshTypes()
    {
        gameSettings.refreshTypeTime = VBRefreshTypes.slider.value;
    }
}
