using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettingsScriptableObject", menuName = "coding-challenge-shapes/GameSettingsScriptableObject", order = 0)]
public class GameSettingsScriptableObject : SingletonScriptableObject<GameSettingsScriptableObject>
{
    public float gameLength = 60f;
    public float objectLifetime = 2f;
    public float refreshTypeTime = 5f;
    public float objectSpawnInterval = 5f;
}
